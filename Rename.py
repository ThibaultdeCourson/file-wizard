from os import listdir, rename, remove, rmdir
from os.path import isfile, isdir, exists, splitext
from datetime import datetime
from pathlib import Path

"""
Functions used
"""


def remove_multiple_spaces(text):
    """
    Remove multiple spaces from string

    :param text: string to process
    :return: processed string
    """

    while text.find("  ") != -1:
        text = text.replace("  ", " ")
    return text


def remove_empty_folder(path):
    """
    Remove the folder if empty

    :param path: path of the folder targeted
    :return: whether the file has been deleted or not
    """

    global elements_deleted

    if len(listdir(path)) == 0:
        elements_deleted += 1
        # folder_path = folder_path[:-1]
        rmdir(path)
        print("Empty folder [" + path + "] deleted")
        return True
    else:
        return False


def remove_trailing(text):
    """
    Remove abnormal trailing characters

    :param text: string to process
    :return: processed string
    """

    characters = {' ', '-', '_'}
    while len(text) > 1 and text[0] in characters:
        text = text[1:]
    while len(text) > 1 and text[len(text) - 1:] in characters:
        text = text[:len(text) - 1]
    return text


def format_name(name, element_type, extension):
    """
    Apply the new format to the name of the element

    :param name: name of the element to modify
    :param element_type: Type of the element: [.] for file, [d] for directory
    :param extension: extension of the element
    :return: the new name of the element
    """

    global changes  # Modifications to apply

    # File types removal
    if element_type == "d" or changes['removal'] is None or \
            (not (len(extension) > 0 and extension[1:] in changes["removal"]) and
             not (len(extension) == 0 and "." in changes["removal"])):

        # Replacements
        if len(changes["replacements"]) != 0:
            for replacement in changes["replacements"]:
                name = name.replace(replacement[0], replacement[1])

        # Films & series modifications
        if changes["video"]:
            # Remove video format information
            for replacement in video_format:
                position = name.lower().find(replacement)
                if position != -1:
                    name = name[:position] + name[position + len(replacement):]

            # Remove brackets and their content
            brackets = (name.find("["), name.find("]"))
            while brackets[0] != -1 and brackets[0] < brackets[1]:
                name = name[:brackets[0]] + name[brackets[1] + 1:]
                brackets = (name.find("["), name.find("]"))

            # Remove year of the film/series
            if changes["year"] and len(name) > 4:
                start = 0
                while start != 1:

                    start = name.find('(', start)  # Search a (

                    # Identify if the ( found is part of an year identifier
                    if start != -1 and start < len(name) - 5:  # Verify if it is the right place in the name

                        if name[start + 1: start + 5].isdigit() and name[start + 5] == ")" and 1892 <= int(
                                name[start + 1: start + 5]) < datetime.now().year:

                            # If yes delete it
                            name = name[:start] + name[start + 6:]
                            break
                        else:
                            # If no search for a new ( in the String
                            start += 1
                    else:
                        break

                start = 0
                while start != 1:
                    start1, start2 = name.find('19', start), name.find('20', start)  # Search a 19 or a 20

                    if start1 != -1 or start2 != -1:

                        if start2 == -1 or (start1 < start2 and start1 != -1):
                            start = start1
                        else:
                            start = start2

                        if start < len(name) - 3:  # Verify if it is the right place in the name

                            if name[start + 2: start + 4].isdigit() and \
                                    int(name[start: start + 4]) <= datetime.now().year:

                                # If yes delete it
                                name = name[:start] + name[start + 4:]
                                break
                            else:
                                # If no search for a new number in the String
                                start += 1
                        else:
                            break
                    else:
                        break

            if changes["series"] and element_type == ".":

                # Correct the episode number (s**e** -> S**E**)
                start = 0
                while start != 1:

                    start = name.find('s', start)  # Search a lowercase S

                    # Identify if the letter found is part of an episode number
                    if start != -1 and start < len(name) - 5:  # Verify if it is the right place in the name

                        if name[start + 3].lower() == "e" and name[start + 1: start + 3].isdigit() \
                                and name[start + 4: start + 6].isdigit():

                            # If yes put an cap S and a cap E
                            name = name[:start] + "S" + name[start + 1: start + 3] + "E" + name[start + 4:]
                            break
                        else:
                            # If no search for a new s in the String
                            start += 1
                    else:
                        break

                # Correct the episode number (**x** -> S**E**)
                start = 0
                while start != 1:
                    start = name.find('x', start)
                    if start != -1 and start < len(name) - 2:

                        # Checks if the letter found is part of an episode number
                        if name[start + 1: start + 3].isdigit() and name[start - 1].isdigit():
                            if start < 2:
                                name = "S0" + name[0] + "E" + name[start + 1:]
                            else:
                                if name[start - 2].isdigit():
                                    name = name[:start - 2] + "S" + name[start - 2:start] + "E" + \
                                           name[start + 1:]
                                else:
                                    name = name[:start - 1] + "S0" + name[start - 1] + "E" + name[start + 1:]
                        else:
                            # If no search for a new x in the String
                            start += 1
                    else:
                        break

                # Add a dash after the episode number
                if changes["dash"]:
                    start = 0
                    while start != -1:
                        start = name.find('S', start)
                        if start != -1 and len(name) - 5 > start:
                            # Identify if the letter found is part of an episode number, if there is no dash after it and if
                            # there is a text following
                            if name[start + 3] == "E" and name[start + 1: start + 3].isdigit() and \
                                    name[start + 4: start + 6].isdigit() and len(name) > start + 6:
                                if len(name) < start + 9:
                                    # If yes add a dash after the episode number
                                    name = name[:start + 6] + " - " + name[start + 6:]
                                elif name[start + 6: start + 9] != " - ":
                                    # If yes add a dash after the episode number
                                    name = name[:start + 6] + " - " + name[start + 6:]
                                break

                            else:
                                # Search for a new S in the String
                                start += 1
                        else:
                            start = -1

            # Replace points by spaces
            if changes["points"]:
                name = name.replace(".", " ")

            # Remove multiple spaces
            name = remove_multiple_spaces(name)

            # Remove empty brackets and parenthesis
            for string_to_remove in ('[]', '[ ]', '()', '( )'):
                while name.find(string_to_remove) != -1:
                    name = name.replace(string_to_remove, "")

            # Remove abnormal trailing characters
            name = remove_trailing(name)

        # Add a suffix to the specified elements
        for fix in ('prefix', 'suffix'):
            for change in changes[fix]:

                if (isinstance(change[1], str) and (change[1] not in {"d", "."} or change[1] == element_type)) \
                        or (len(extension) > 0 and extension[1:] in change[1]):

                    if len(name) > len(change[0]):

                        # Check if the pref/suf.fix is not already in the name...
                        if name[len(name) - len(change[0]):] != change[0]:
                            name += change[0]  # ... if not add it
                    else:
                        name += change[0]

        # Remove multiple spaces and abnormal trailing characters
        if changes['format']:
            name = remove_trailing(remove_multiple_spaces(name))

        return name
    else:
        return None


def browse_folder(folder_path):
    """
    Apply the new name format to the elements of a directory

    :param folder_path: The location of the directory to browse
    :return: False if the folder have been deleted
    """

    global corrections
    global elements_deleted
    element_names = listdir(folder_path)  # List the contents of the directory

    folder_path += "\\"

    for element_name in element_names:  # Inspect each element of the directory

        new_element_name, extension = splitext(element_name)
        element_type = ''

        # If the element is a file (not a folder) remove the extension of the name
        if isfile(folder_path + element_name):
            element_type = '.'
        elif isdir(folder_path + element_name):
            element_type = 'd'

        # Format the name
        # print("[", new_element_name, "] [", element_type, "] [", extension, "]", sep='')
        if element_name not in changes['protect']:
            new_element_name = format_name(new_element_name, element_type, extension)

            if new_element_name is None:
                elements_deleted += 1
                # print("Trying to remove [", folder_path + element_name, "]", sep='')
                remove(folder_path + element_name)
                print("[" + element_name + "] deleted")

            else:

                # Put back the extension of the filename
                new_element_name = new_element_name + extension

                # If there is, apply the modifications
                if new_element_name != element_name:

                    if exists(folder_path + new_element_name):
                        print("[" + element_name + "] cannot be renamed [" + new_element_name +
                              "], a file with the same name already exists")
                    else:
                        # print("Trying to rename [" + element_name + "] to [" + new_element_name + "]")
                        try:
                            rename(folder_path + element_name, folder_path + new_element_name)
                        except PermissionError:
                            print("Impossible to rename [", folder_path + element_name, "] to [", folder_path +
                                  new_element_name, "]\nThe element might be currently open by an app", sep='')

                        corrections += 1
                        print("[" + element_name + "] successfully renamed [" + new_element_name + "]")

                # If it is a folder look inside
                if isdir(folder_path + new_element_name):
                    if browse_folder(folder_path + new_element_name):
                        if changes['flatten'] and new_element_name != "Subs":
                            for sub_element_name in listdir(folder_path + new_element_name):
                                if not exists(folder_path + sub_element_name):
                                    rename(folder_path + new_element_name + '\\' + sub_element_name,
                                           folder_path + sub_element_name)
                            remove_empty_folder(folder_path + new_element_name)

    return not remove_empty_folder(folder_path)


"""
Core: input of the changes requested
"""

print("\nWELCOME to the name editor program.\n" +
      "This program allows to easily format the name of multiple files and folders")

exit_prog = False

video_format = ["aac2.0", "aac", "amzn", ".amazon.", ".am.", "amrap", "[.ag]", "eac3", "ac3", "av1",
                "bluray", "brrip", "-blutonium", "-bia", "bokutox", "bitloks", "bdrip", "bugsfunny",
                "custhome", "censored", "-ct", "-cinefile",
                "ddp5.1", ".dd.", "dd5.1", "dryb", "dsnp", "dvdrip", "dvd", "director.cut", "deef", "deceit", ".dts."
                "eng-ita", "esub",
                "french", "fs96 Joy", "fs96", "-flux", "-ffg",
                "-greenblade", "galaxyrg",
                "-hwd", "h.264", "h264", "hdtv", "hdts", "hevc", "h265", "hushrips",
                "internal", "imax",
                "jason28", "justiso",
                "-ksrm", "kontrast",
                "-less",
                "msubs", ".mx", "-mrsk", "maximersk", "mrsktv", "-mixed",
                "ntsc", ".nf", "-ntb", "-NTb",
                "-organic", "-oosh",
                "-phoenix", "proper", "-poiasd", "peculate",
                "rarbg", "rartv", "remastered", "rlsd", "rcvr", "-rovers", "-riprg", "repack", "rzerox",
                "_sujaidr", "-shortbrehd", "swesub", "-shiv@",
                "tommy", "trollhd", "teneighty", "-tepes", "tgx", "t3nzin",
                "[utr]", "unrated", "unrated.dc",
                "v2",
                "x264", "x265 Silence", "x265",
                "web-dl", "webrip", "web", "yts", "yify",
                "1080p", "10bit", "4k", "5.1", "2.0", "720p", "-7sins", "19s20e1080"]

while not exit_prog:

    changes = {"prefix": [],
               "suffix": [],
               "replacements": [],
               "removal": [],
               'protect': []}

    # Select the target folder
    basepath = input("\n\nEnter the directory path (press ENTER to use the current directory): ")  # The target

    if basepath == "":  # If no entry use the current directory
        basepath = str(Path().absolute())

    print("Path [", basepath, "] selected", sep='')

    # Basic modifications

    # Add prefixes and suffixes

    for fix in ('prefix', 'suffix'):
        user_input = " "
        while user_input != "":
            user_input = input("\nAdd a " + fix + ": (Press ENTER to add nothing): ")
            if user_input != "":
                sec_user_input = input("To what type of element? (ENTER for all, [d] for directory, " +
                                       "[.] for any file, or type the extension targeted) ")
                if sec_user_input not in ("", "d", "."):
                    sec_user_input = [sec_user_input]
                    terc_user_input = " "
                    while terc_user_input != "":
                        terc_user_input = input("Other extension targeted (or ENTER if no further additions required):")
                        if terc_user_input != "":
                            sec_user_input.append(terc_user_input)
                changes[fix].append((user_input, sec_user_input))

    # Replacements
    to_replace = "a"
    while to_replace != "":
        to_replace = input("\nText to replace (or ENTER if not necessary): ")
        if to_replace != "":
            changes["replacements"].append([to_replace, input("Replace it by: ")])

    # Removing files based on type
    to_replace = "a"
    print('\n(ex: "png", or "." for files with no extension)')
    while to_replace != "":
        to_replace = input("File type to remove (or ENTER if not necessary): ")
        if to_replace != "":
            changes["removal"].append(to_replace)

    # Films and series modifications
    changes["video"] = input("\nIs it a film or a series? (ENTER = yes, anything = no): ") == ""

    if changes["video"]:

        changes['protect'] += ['.idea', '.git', '.gitignore', 'requirements']
        changes['removal'] += ["txt", "jpg", "exe", "nfo"]

        changes["year"] = input("Remove the year (contained in parentheses)? (ENTER = yes, anything = no): ") == ""

        changes["series"] = input("Is it a series? (ENTER = yes, anything = no): ") == ""

        if changes["series"]:
            # Add a dash
            changes["dash"] = input('Series: Add a dash between the number and the name of the episode ' +
                                    '("S**E** - *") ? (ENTER = yes, anything = no): ') == ""

        # Remove points
        changes["points"] = input("\nRemove points? (ENTER = yes, anything = no): ") == ""

    # Protect
    to_replace = "a"
    while to_replace != "":
        to_replace = input("\nElements to preserve (or ENTER if not necessary): ")
        if to_replace != "":
            changes["preserve"].append(to_replace)

    # Format modifications
    changes["format"] = input('\nRemove double space and abnormal trailing characters (space, dash, underscore)? ' +
                              '(ENTER = yes, anything = no): ') == ""

    # Flatten hierarchy
    changes["flatten"] = input(
        "\nFlatten folder hierarchy? (ENTER = yes, anything = no): ") == ""

    """
    Summary of the changes requested
    """

    print("\n\nDirectory targeted: [", basepath, "]\nModifications:", sep='')

    for fix in ('prefix', 'suffix'):
        for change in changes[fix]:
            print("- Add [" + change[0] + "]",
                  "in front" if fix == 'prefix' else "at the end",
                  "of every ", end='')

            if isinstance(change[1], str):
                if change[1] == "d":
                    print("directory name")
                elif change[1] == ".":
                    print("file name")
                else:
                    print("element name")
            else:
                print("file with a", ", ".join(change[1]), "extension")

    if changes["video"]:
        print("- Films & series: removing format information (720p, etc.)")

        if changes["year"]:
            print("- Films & series: removing the year")

        if changes["series"]:
            print("- Series episodes: formating episode number")

            if changes["dash"]:
                print("- Series episodes: add a dash between the number and the name of the episode")

        if changes["points"]:
            print("- Replace [.] by [ ]")

    for change in changes["replacements"]:
        print("- Replace [" + change[0] + "] by [" + change[1] + "]")

    if len(changes["removal"]) > 0:
        print("- Remove files with extensions " + ', '.join(changes["removal"]))

    if changes["format"]:
        print("- Remove multiple space and abnormal trailing characters")

    if changes["flatten"]:
        print("- Flatten folder hierarchy\n(Keeps the folder in case of name conflict)")

    confirmation = True if input("Confirm ? (ENTER = yes, anything = no) ") == "" else False

    """
    Execution (or not) of the modifications
    """

    if confirmation:
        print("\nProcessing...")

        corrections = 0
        elements_deleted = 0

        # sep = basepath.rfind("\\")  # Find the last / in the path
        # format_name(basepath[sep + 1:], basepath[:sep + 1])

        browse_folder(basepath)
        print("\n[", basepath, "] successfully cleaned. ", corrections, " elements modified.", sep='')

    exit_prog = True if input("\nExit program or make a new modification ? (ENTER = exit, anything = retry) ") == "" \
        else False
